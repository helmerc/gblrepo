# README #

* This program is a tool to help answer frequently asked questions on a mock request for proposal for a fake company named Global Business Logistics (Any similarity to existing companies is purely coincidental). 

### How do I get set up? ###

* Run GUIFrame.java
* user name: helmerc
* password: 1234

## Authors ##

* Christopher Helmer
* Kimberly Stewart
* Brandon Lioce
* Dan Taylor

### Who do I talk to? ###

* Chris Helmer
* christopher.helmer@yahoo.com